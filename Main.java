package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner weather = new Scanner(System.in);
        System.out.println("Введите температуру");
        float temperature = weather.nextFloat();
        System.out.println("Введите скорость ветра");
        float windVelocity = weather.nextFloat();
        System.out.println("Введите 'yes', если на улице дождь, иначе - 'no' ");
        String rain = weather.next();
        if (temperature > 18 & windVelocity < 10 & rain.equals("no")) {
            System.out.println("Отличная погода, можно гулять");
        } else if (temperature < 18 & windVelocity < 10 & rain.equals("no")) {
            System.out.println("Немного прохладно, можно гулять");
        } else if (temperature < 18 & windVelocity > 10 & rain.equals("no")) {
            System.out.println("Немного прохладно, да и ветер сильный, сидите дома");
        } else if (temperature < 18 & windVelocity > 10 & rain.equals("yes")) {
            System.out.println("Плохая погода сидите дома");
        } else if (temperature > 18 & windVelocity < 10 & rain.equals("yes")) {
            System.out.println("На улице тепло, но идет дождь");
        } else if (temperature > 18 & windVelocity > 10 & rain.equals("yes")) {
            System.out.println("Дождь и ветер");

        } else {
            System.out.println("Думаю можно погулять");
        }
    }

}